package Problem15;

import java.util.Scanner;

public class ProductLogic {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String userChoice = null;
		Product[] refProducts = new Product[0];
		int totalPrice = 0; int flatDiscount = 20;
		
		do {
			Scanner sc = new Scanner(System.in);
			
			System.out.print("Enter Product ID: ");
			int productID = sc.nextInt();
			sc.nextLine();//consume the "\n";

			System.out.println(" ");
			System.out.print("Product Name: ");
			String productName = sc.nextLine();
			
			System.out.println(" ");
			System.out.print("Price in SGD: ");
			int price = sc.nextInt();
			
			System.out.println(" ");
			System.out.print("Quantity: ");
			int quantity = sc.nextInt();
			
			totalPrice += price * quantity;
			Product newProduct = new Product(productID, productName, price, quantity);
			refProducts = addElement(refProducts, newProduct);
			
			System.out.println(" ");
			System.out.print("Wish to Continue?(Y/N): ");
			userChoice = sc.next();
			System.out.println(" ");
		} while( !(userChoice.equals("N")));
		
		System.out.println("Your Product list is as follows : ");
		System.out.println(" ");
		System.out.printf("\t %-15s %-15s %-10s %-10s \n", "ProductID", "Product Name", "Price", "Quantity");
		
		for (Product product: refProducts) {
			System.out.printf("\t %-15d %-15s %-10d %-10d \n", product.productID, product.productName, product.price, product.quantity);
		}
		double discountAmt = 0.2 * totalPrice;
		
		System.out.println("");
		System.out.printf("\t %-15s %-15d \n", "Total Price", totalPrice);
		System.out.printf("\t %-15s %-15s \n", "Flat Discount", flatDiscount+ "%");
		System.out.printf("\t %-15s %-15.2f \n", "Discount Amount", discountAmt);
		System.out.printf("\t %-15s %-15.2f \n", "Amount To Pay", totalPrice - discountAmt);
		
	}
	
	
	public static Product[] addElement(Product[] refProducts, Product newProduct) {
		Product[] newRefProducts = new Product[refProducts.length + 1];
		for( int i = 0; i < refProducts.length; i++) {
			newRefProducts[i] = refProducts[i];
		}
		newRefProducts[newRefProducts.length-1] = newProduct;
		
		return newRefProducts;
	}

}
