package Problem15;

public class Product {
	
	int productID;
	String productName;
	int price;
	int quantity;
	
	Product(int productID, String productName, int price, int quantity) {
		this.productID = productID;
		this.productName = productName;
		this.price = price;
		this.quantity = quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
//	@Override
//	public String toString() {
//		return this.productID + "        " + this.productName + "  " + this.price + "    " + this.quantity;
//	}

}
