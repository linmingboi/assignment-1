package com.optimum.javabasics;

import java.util.Scanner;

public class Problem3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("How many rows do you want to print? : ");
		int intRef = sc.nextInt(); // Collecting user input on the number of rows he or she wants
		
		for(int i = 0; i < intRef; i++) {
			String output = "";
			int currentRef = 65;
			char currentChar = (char)(currentRef + i);
			for(int j = 0; j <= i; j++) {
				
				output += currentChar + " ";
			}
			System.out.println(output);
		}	
		System.out.println();
	}

}
