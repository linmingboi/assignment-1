package com.optimum.javabasics;

import java.util.Scanner;

public class Problem16 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of Elements you want to see in the Fibonacci Series");
		int numberOfElements = sc.nextInt(), num1 = 0, num2 = 1;
		forLoopFibo(numberOfElements, num1, num2);
		whileLoopFibo(numberOfElements, num1, num2);
		System.out.println("Enter a value in the Fibonacci Series instead");
		int value = sc.nextInt();
		givenValueFibo(value, num1, num2);
		
	}
	
	public static void forLoopFibo(int numberOfElements, int num1, int num2) {
		String fiboSeq = "The Fibonacci Sequence: 0, 1";
		for(int i = 0; i < numberOfElements - 2; i++) {
			int sumOfTwoNums = num1 + num2;
			num1 = num2;
			num2 = sumOfTwoNums;
			fiboSeq += ", " + sumOfTwoNums; 
		}
		System.out.println(fiboSeq);
	}
	
	public static void whileLoopFibo(int numberOfElements, int num1, int num2) {
		String fiboSeq = "The Fibonacci Sequence: 0, 1";
		int count = 2;
		while(count++ != numberOfElements) {
			int sumOfTwoNums = num1 + num2;
			num1 = num2;
			num2 = sumOfTwoNums;
			fiboSeq += ", " + sumOfTwoNums; 
		}
		System.out.println(fiboSeq);
	}
	
	public static void givenValueFibo(int value, int num1, int num2) {
		String fiboSeq = "The Fibonacci Sequence: 0, 1";

		while((num1+num2) != value) {
			int sumOfTwoNums = num1 + num2;
			num1 = num2;
			num2 = sumOfTwoNums;
			fiboSeq += ", " + sumOfTwoNums; 
		}
		System.out.println(fiboSeq + ", " + value );
	}

}
