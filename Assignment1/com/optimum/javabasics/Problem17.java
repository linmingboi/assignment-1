package com.optimum.javabasics;

import java.util.Scanner;

public class Problem17 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number: ");
		int factorialNum = sc.nextInt();
		int count = 1;
		int temp = factorialNum;
		for( int i = 1; i <= temp; i++) {
			count *= i;
		}
		System.out.println("Factorial of " + factorialNum + "  using for loop is " + count);
		int count1 = 1;
		int temp1 = factorialNum;
		while( temp1 > 1 ) {
			count1 *= temp1;
			temp1 --;
		}
		System.out.println("Factorial of " + factorialNum + "  using while loop is " + count1);
		System.out.println("Factorial of " + factorialNum + " using recursion is " + recursionMethod( factorialNum) );
		
	}
	
	public static int recursionMethod(int factorialNum) {
		if( factorialNum == 1) {
			return 1;
		}
		return factorialNum * recursionMethod(factorialNum - 1);
		
	}
}
