package com.optimum.javabasics;

import java.util.Scanner;

public class Problem20 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a Palindrome String sequence");
		String value = sc.nextLine();
		usingReverse(value);
		if(notUsingReverse(value)) {
			System.out.println("The string " + value + " is a palindrome using non-library built in method");
		} else {
			System.out.println("The string " + value + " is not a palindrome using non-library built in method");
		}
	}
	
	public static void usingReverse(String value) {
		StringBuilder reverseString = new StringBuilder();
		reverseString.append(value).reverse();
		
		if(reverseString.toString().equals(value)) {
			System.out.println("The string " + value + " is a palindrome using the reverse method");
		} else {
			System.out.println("The string " + value + " is not a palindrome using the reverse method");
		}
	}
	
	public static boolean notUsingReverse(String value) {
		int i = 0, j = value.length() - 1;
		
		while( i < j ) {
			if(value.charAt(i) != value.charAt(j)) {
				return false;
			} 
			i++;
			j--;
		}
		return true;
	}
}
