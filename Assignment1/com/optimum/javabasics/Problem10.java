package com.optimum.javabasics;

public class Problem10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] input1 = {1,5,10,20,40,80};
		int[] input2 = {6,7,20,80,100};
		int[] input3 = {3,4,15,20,30,70,80,120};
		findCommon(input1, input2, input3);
		
	}
	
	public static void findCommon(int[] arr1,int[] arr2, int[] arr3) {
		int i = 0, j = 0, k = 0;
		String commonElements = "Output: ";
		while( i < arr1.length && j < arr2.length && k < arr3.length) {
			if( arr1[i] == arr2[j] && arr2[j] == arr3[k]) {
				commonElements += arr1[i] + ",";
				i++; j++; k++;
			} else if( arr1[i] < arr2[j] ) {
				i++;
			} else if( arr2[j] < arr3[k]) {
				j++;
			} else {
				k++;
			}
		}
		System.out.println(commonElements.substring(0, commonElements.length() - 1));
	}
}
