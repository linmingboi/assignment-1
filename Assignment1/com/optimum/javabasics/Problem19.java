package com.optimum.javabasics;

import java.util.Scanner;

public class Problem19 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a Palindrome number sequence");
		int value = sc.nextInt();
		whileLoopPalindrome(value);
		forLoopPalindrome(value);
	}
	
	public static void whileLoopPalindrome(int value) {
		int comparisonValue = value;
		int sum = 0;
		int remainder = 0;
		while(value > 0) {
			remainder = value % 10;
			sum = (sum*10) + remainder;
			value = value / 10;
		}
		if(comparisonValue == sum) {
			System.out.println("The sequence " + comparisonValue + " using while loop to check is a palindrome");
		} else {
			System.out.println("The sequence " + comparisonValue + " using while loop to check is not a palindrome");
		}
	}
	
	public static void forLoopPalindrome(int value) {
		int comparisonValue = value;
		int sum = 0;
		int remainder = 0;
		for(; value != 0;) {
			remainder = value % 10;
			sum = (sum*10) + remainder;
			value = value / 10;
		}
		if(comparisonValue == sum) {
			System.out.println("The sequence " + comparisonValue + " using for loop to check is a palindrome");
		} else {
			System.out.println("The sequence " + comparisonValue + " using for loop to check is not a palindrome");
		}
	}

}
