package com.optimum.javabasics;

import java.util.Arrays;
import java.util.Scanner;

public class Problem12 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of elements you want to store");
		int arrLength = sc.nextInt();
		int[] arr = new int[arrLength];
		
		System.out.println("Enter the elements you want in your array: ");
		for(int i = 0; i < arrLength; i++) {
			System.out.println("Enter the number " + (i+1) + " element of the array");
			arr[i] = sc.nextInt();
		}
		
		Arrays.sort(arr);
		System.out.println("Second Smallest Element: " + arr[1]);
		System.out.println("Second Largest Element: " + arr[arrLength-2]);
	}
}
