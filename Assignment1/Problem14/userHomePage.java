package Problem14;

import java.util.Scanner;

import Problem15.Product;

public class userHomePage {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("User Home Page: \n 1.Register \n 2.Login \n 3.Forget Password \n 4.Logout(exit)");
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter Your Choice: ");
		int choice = sc.nextInt();
		
		User[] userArr = new User[0];
		User newUser = new User("xyz@gmail.com", "password", "black");
		userArr = addElement(userArr, newUser);
		System.out.println(userArr[0].userID);
		while( choice != 4 ) {
			if( choice > 4 || choice < 1) {
				System.out.println("Choice not available");
				System.out.print("Enter your choice: ");
				choice = sc.nextInt();
				sc.nextLine();
				continue;
			}
			if( choice == 1 ) {
				System.out.print("Enter email address: ");
				String newEmail = sc.nextLine();
				sc.nextLine();
				boolean emailResult = checkExistingEmail(newEmail, userArr);
				while(emailResult) {
					System.out.println("Email already exists");
					System.out.print("Enter email address: ");
					newEmail = sc.nextLine();
					emailResult = checkExistingEmail(newEmail, userArr);
				}
				System.out.print("Enter Password: ");
				String newPassword = sc.nextLine();
				System.out.print("Re-type Password: ");
				String retypePassword = sc.nextLine();
				boolean passWordResult = passWordMatch(newPassword, retypePassword);
				while(!passWordResult) {
					System.out.println("Password doesnt match");
					System.out.print("Re-type Password: ");
					retypePassword = sc.nextLine();
					passWordResult = passWordMatch(newPassword, retypePassword);
				}
				System.out.println("What is your favourite colour? ");
				String securityKey = sc.nextLine();
				System.out.println(securityKey + " is your security key, incase you forget your password.");
				newUser = new User(newEmail, newPassword, securityKey);
				userArr = addElement(userArr, newUser);
				System.out.println("Registration Successful");
			}
			
			if(choice == 2) {
				System.out.print("Enter UserID: ");
				String userID = sc.nextLine();
				sc.nextLine();
				System.out.print("Password");
				String userPassword = sc.nextLine();
				User user = checkLogin(userID, userPassword, userArr) ;
				if(user != null) {
					System.out.println("Login SuccessFul");
					System.out.println("User Home Page: \n 1.Check Available Bank Balance \n 2.Deposit Amount \n 3.Withdraw Amount");
					System.out.print("Enter your Choice");
					int userInput = sc.nextInt();
					String checkOut = "";
					do {
						if( userInput > 3 || userInput < 1) {
							System.out.println("Choice not available");
							System.out.print("Enter your choice: ");
							userInput = sc.nextInt();
							sc.nextLine();
							continue;
						}
						if(userInput == 1) {
							System.out.println("Available balance: " + user.availBalance);
						} else if( userInput == 2) {
							System.out.println("Enter Amount");
							int amount = sc.nextInt();
							sc.nextLine();
							while(amount <0) {
								System.out.println("Amount cant be negative");
								System.out.println("Enter Amount");
								amount = sc.nextInt();
								sc.nextLine();
							}
							
							user.availBalance += amount;
							System.out.println(amount + " dollars deposited successfully!");	
						
						} else if( userInput == 3) {
							System.out.print("Enter amount");
							int amountToBeDeducted = sc.nextInt();
							sc.nextLine();
							if( amountToBeDeducted > user.availBalance ) {
								System.out.println("Insufficient Balance");
							} else {
								user.availBalance -= amountToBeDeducted;
								System.out.println(user.availBalance);
								System.out.println("Transaction successful");
							}
						} 
						System.out.print("Wish to Continue?(y/n)");
						checkOut = sc.next();
						if(!checkOut.equals("n")) {
							System.out.print("Enter Your Choice: ");
							userInput = sc.nextInt();
						} else {
							System.out.println("Thanks for banking with us");
						}
					} while(!checkOut.equals("n"));
				} else {
					System.out.println("Wrong Username or password");
				}
			}
			if(choice == 3) {
				System.out.print("Enter your ID: ");
				String userID = sc.nextLine();
				sc.nextLine();
				System.out.print("Enter you security key: ");
				String securityKey = sc.nextLine();
				User userRef = resetUser(userID, securityKey, userArr);
				System.out.print("Enter Password: ");
				String newPassword = sc.nextLine();
				System.out.print("Re-type Password: ");
				String retypePassword = sc.nextLine();
				boolean passWordResult = passWordMatch(newPassword, retypePassword);
				while(!passWordResult) {
					System.out.println("Password doesnt match");
					System.out.print("Re-type Password: ");
					retypePassword = sc.nextLine();
					passWordResult = passWordMatch(newPassword, retypePassword);
				}
				userRef.password = newPassword;
				System.out.println("What is your favourite colour? ");
				securityKey = sc.nextLine();
				userRef.securityKey = securityKey;
				System.out.println(securityKey + " is your security key, incase you forget your password.");
				System.out.println("Your password has been reset successfully");
				
			}
			
		
			System.out.println("User Home Page: \n 1.Register \n 2.Login \n 3.Forget Password \n 4.Logout(exit)");
			sc = new Scanner(System.in);
			
			System.out.print("Enter Your Choice: ");
			choice = sc.nextInt();
		}
		System.out.println("Logout Successfully!");
	}
	
	public static User resetUser(String userID, String securityKey, User[] userArr) {
		for(User user: userArr) {
			if(user.userID.equals(userID)  && user.securityKey.equals(securityKey)) {
				return user;
			}
		}
		return null;
	}

	public static User checkLogin(String userID, String password, User[] userArr) {
		for(User user: userArr) {
			if(user.userID.equals(userID) && user.password.equals(password)) {
				return user;
			}
		}
		return null;
	}
	public static boolean checkExistingEmail(String newEmail, User[] userArr) {
		if(userArr.length == 0) {
			System.out.println("It went into first loop");
			return false;
		}
		for( User user: userArr) {
			System.out.println(user.userID);
			System.out.println(newEmail);
			if(user.userID.equals(newEmail)) {
				System.out.println("It went into the truth block");
				return true;
			}
		}
		return false;
	}
	
	public static boolean passWordMatch(String newPassword, String retypePassword) {
		if(newPassword.equals(retypePassword)) {
			return true;
		}
	
		return false;
	}
	
	public static User[] addElement(User[] userArr, User newUser) {
		User[] newUserArr = new User[userArr.length + 1];
		for( int i = 0; i < userArr.length; i++) {
			newUserArr[i] = userArr[i];
		}
		newUserArr[newUserArr.length-1] = newUser;
		
		return newUserArr;
	}

}
