import java.util.Arrays;
import java.util.Scanner;

public class Problem13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of elements you want to store");
		int arrLength = sc.nextInt();
		int[] duplicateArr = new int[arrLength];
		
		System.out.println("Enter the elements you want in your array: ");
		for(int i = 0; i < arrLength; i++) {
			System.out.println("Enter the number " + (i+1) + " element of the array");
			duplicateArr[i] = sc.nextInt();
		}
		System.out.println("Input array elements: \n" + Arrays.toString(duplicateArr));
		
		Arrays.sort(duplicateArr);
		int uniqueCount = 0;
		for(int i = 0; i < arrLength; i++) {
			if(i == 0) {
				uniqueCount++;
				continue;
			} else {
				if( duplicateArr[i] != duplicateArr[i-1]) {
					uniqueCount++;
				}
			}
		}
		System.out.println("Length of the new array is " + uniqueCount);
	}

}
